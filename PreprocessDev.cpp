//
// Created by Anh Khoa on 8/15/16.
//

#include "PreprocessDev.h"

#include <stdio.h>
#include <algorithm>
#include <GfxState.h>
#include <GfxFont.h>

#define kRound(x) ((int)(x + 0.5))

extern GBool showHidden;

template<typename T>
typename std::vector<T>::iterator insert_sorted(std::vector<T> &vec, T const &item) {
    return vec.insert
            (
                    std::upper_bound(vec.begin(), vec.end(), item),
                    item
            );
}

PreprocessDev::PreprocessDev() {
    marginLeft = 1;
    marginTop = 1;
    willRecordMarginLeft = false;
    calculated = false;
    lastLineY = -1;
    newPage = true;
    changedFont = true;
}

void PreprocessDev::calculate() {
    if (calculated) {
        return;
    }

    calculated = true;

    int max1 = 0, countMax1 = 0,
            max2 = 0, countMax2 = 0,
            max3 = 0, countMax3 = 0,
            cur = 0, countCur = 0;

    for (int i = 0; i < lefttop.size(); i++) {
        countCur++;
        cur = lefttop[i].first;
        if (i == lefttop.size() - 1 || cur != lefttop[i + 1].first) {
//            printf("cur: %d, value: %d\n", countCur, cur);
            if (countCur > countMax1) {
                countMax3 = countMax2;
                max3 = max2;

                countMax2 = countMax1;
                max2 = max1;

                countMax1 = countCur;
                max1 = cur;
            } else if (countCur > countMax2) {
                countMax3 = countMax2;
                max3 = max2;

                countMax2 = countCur;
                max2 = cur;

            } else if (countCur > countMax3) {
                countMax3 = countCur;
                max3 = cur;
            }

            countCur = 0;
        }
    }

    if (max2 > max3) {
        int tmp = max3;
        max3 = max2;
        max2 = tmp;

        tmp = countMax3;
        countMax3 = countMax2;
        countMax2 = tmp;
    }

    if (max1 > max3) {
        int tmp = max1;
        max1 = max2;
        max2 = max3;
        max3 = tmp;

        tmp = countMax1;
        countMax1 = countMax2;
        countMax2 = countMax3;
        countMax3 = tmp;
    } else if (max1 > max2) {
        int tmp = max1;
        max1 = max2;
        max2 = tmp;

        tmp = countMax1;
        countMax1 = countMax2;
        countMax2 = tmp;
    }

//    printf("1 max: %d, value: %d\n", countMax1, max1);
//    printf("2 max: %d, value: %d\n", countMax2, max2);
//    printf("3 max: %d, value: %d\n", countMax3, max3);

    marginLeft = max1;

    std::remove_if(lefttop.begin(), lefttop.end(),
                   [&max1](std::pair<int, int> x) { return x.first != max1; });

    marginTop = std::min(lefttop[0].second, marginTop);

    lefttop.clear();

    /**
     * Calculate common spacing
     */
    countCur = 0;
    int commonSpacing = 0;
    int countCommon = 0;

    for (int i = 0; i < spacing.size(); i++) {
        countCur++;
        cur = spacing[i];
        if (i == spacing.size() - 1 || cur != spacing[i + 1]) {
//            printf("count: %d, value: %d\n", countCur, cur);
            if (countCur > countCommon) {
                countCommon = countCur;
                commonSpacing = cur;
            }

            countCur = 0;
        }
    }

    this->commonSpacing = commonSpacing;

    spacing.clear();
}

void PreprocessDev::startPage(int pageNum, GfxState *state, XRef *xref) {
    newPage = true;
}

void PreprocessDev::endPage() {

}

void PreprocessDev::beginString(GfxState *state, GooString *s) {
    willRecordMarginLeft = true;
}

void PreprocessDev::endString(GfxState *state) {

}

void PreprocessDev::drawChar(GfxState *state, double x, double y,
                             double dx, double dy,
                             double originX, double originY,
                             CharCode code, int /*nBytes*/, Unicode *u, int uLen) {
    if (!showHidden && (state->getRender() & 3) == 3) {
        return;
    }

    if (willRecordMarginLeft && kRound(y - lastLineY) != 0) {
        /**
         * Spacing
         */
        if (newPage) {
            newPage = false;
        } else {
            insert_sorted(spacing, kRound(y - lastLineY));
        }

        /**
         * Margin left ,margin top
         */
//        printf("x: %f, y: %f, dx: %d, dy: %d, X: %d, Y: %d\n", x, y, (int) dx, (int) dy, (int) originX, (int) originY);
        lastLineY = y;

        double x1, y1;
        state->transform(x, y, &x1, &y1);

        insert_sorted(lefttop, {kRound(x1), kRound(y1)});
        willRecordMarginLeft = false;

        GfxFont *font = state->getFont();

        state->transform(state->getCurX(), state->getCurY(), &x1, &y1);

        double fontSize = state->getTransformedFontSize();
        double ascent = font->getDescent();
        int curMaxTop = (int) (y1 - ascent * fontSize);

        if (marginTop == 1) {
            marginTop = curMaxTop;
        } else {
            marginTop = std::min(curMaxTop, marginTop);
        }
    }
}

void PreprocessDev::updateFont(GfxState *state) {
    changedFont = true;
}

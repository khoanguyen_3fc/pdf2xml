//
// Created by Anh Khoa on 5/31/16.
//

#ifndef PDF2XMLEX_XMLUNDERLINEDS_H
#define PDF2XMLEX_XMLUNDERLINEDS_H

#include <stdlib.h>
#include <string.h>
#include <vector>
#include "goo/GooString.h"

class XmlUnderlined {

private:
    double Xmin;
    double Ymin;
    double Xmax;
    double Ymax;

public:
    XmlUnderlined(const XmlUnderlined &x);

    XmlUnderlined(double xmin, double ymin, double xmax, double ymax);

    ~XmlUnderlined();

    double getX1() const { return Xmin; }

    double getX2() const { return Xmax; }

    double getY1() const { return Ymin; }

    double getY2() const { return Ymax; }

    GBool inUnderlined(double xmin, double ymin, double xmax, double ymax) const;
};

class XmlUnderlineds {
private:
    std::vector<XmlUnderlined> *accu;
public:
    XmlUnderlineds();

    ~XmlUnderlineds();

    void AddUnderlined(const XmlUnderlined &x) { accu->push_back(x); }

    GBool inUnderlined(double xmin, double ymin, double xmax, double ymax) const;
};


#endif //PDF2XMLEX_XMLUNDERLINEDS_H

//
// Created by Anh Khoa on 8/15/16.
//

#ifndef PDF2XMLEX_PREPROCESSDEV_H
#define PDF2XMLEX_PREPROCESSDEV_H

#include <poppler/OutputDev.h>
#include <vector>

class PreprocessDev : public OutputDev {
public:
    PreprocessDev();

    void calculate();

    int getMarginLeft() { return marginLeft; };

    int getMarginTop() { return marginTop; };

    int getSpacing() { return commonSpacing; };

    // Does this device use upside-down coordinates?
    // (Upside-down means (0,0) is the top left corner of the page.)
    virtual GBool upsideDown() { return gTrue; }

    // Does this device use drawChar() or drawString()?
    virtual GBool useDrawChar() { return gTrue; }

    // Does this device use beginType3Char/endType3Char?  Otherwise,
    // text in Type 3 fonts will be drawn with drawChar/drawString.
    virtual GBool interpretType3Chars() { return gFalse; }

    // Does this device need non-text content?
    virtual GBool needNonText() { return gTrue; }


    // Start a page.
    virtual void startPage(int pageNum, GfxState *state, XRef *xref);

    // End a page.
    virtual void endPage();

    //----- text drawing
    virtual void beginString(GfxState *state, GooString *s);

    virtual void endString(GfxState *state);

    virtual void drawChar(GfxState *state, double x, double y,
                          double dx, double dy,
                          double originX, double originY,
                          CharCode code, int nBytes, Unicode *u, int uLen);

    //----- update text state
    virtual void updateFont(GfxState *state);

private:
    int marginLeft;
    int marginTop;
    std::vector<std::pair<int, int>> lefttop;
    bool willRecordMarginLeft;
    bool calculated;
    double lastLineY;
    bool newPage;
    bool changedFont;
    std::vector<int> spacing;
    int commonSpacing;
};


#endif //PDF2XMLEX_PREPROCESSDEV_H

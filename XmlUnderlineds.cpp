//
// Created by Anh Khoa on 5/31/16.
//

#include "XmlUnderlineds.h"

XmlUnderlined::XmlUnderlined(const XmlUnderlined &x) {
    Xmin = x.Xmin;
    Ymin = x.Ymin;
    Xmax = x.Xmax;
    Ymax = x.Ymax;
}

XmlUnderlined::XmlUnderlined(double xmin, double ymin, double xmax, double ymax) {
    if (xmin < xmax) {
        Xmin = xmin;
        Xmax = xmax;
    } else {
        Xmin = xmax;
        Xmax = xmin;
    }
    if (ymin < ymax) {
        Ymin = ymin;
        Ymax = ymax;
    } else {
        Ymin = ymax;
        Ymax = ymin;
    }
}

XmlUnderlined::~XmlUnderlined() {
}

GBool XmlUnderlined::inUnderlined(double xmin, double ymin, double xmax, double ymax) const {
    double Y = Ymin;
    // TODO: fix underlined
    return (ymin < Y) && (ymax > Y) && (xmin >= Xmin - 0.5) && (xmax <= Xmax + 0.5);
}


XmlUnderlineds::XmlUnderlineds() {
    accu = new std::vector<XmlUnderlined>();
}

XmlUnderlineds::~XmlUnderlineds() {
    delete accu;
    accu = NULL;
}

GBool XmlUnderlineds::inUnderlined(double xmin, double ymin, double xmax, double ymax) const {

    for (std::vector<XmlUnderlined>::iterator i = accu->begin(); i != accu->end(); ++i) {
        if (i->inUnderlined(xmin, ymin, xmax, ymax)) {
            return 1;
        }
    }
    return 0;
}

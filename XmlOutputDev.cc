//========================================================================
//
// XmlOutputDev.cc
//
// Copyright 1997-2002 Glyph & Cog, LLC
//
// Changed 1999-2000 by G.Ovtcharov
//
// Changed 2002 by Mikhail Kruk
//
//========================================================================

//========================================================================
//
// Modified under the Poppler project - http://poppler.freedesktop.org
//
// All changes made under the Poppler project to this file are licensed
// under GPL version 2 or later
//
// Copyright (C) 2005-2013 Albert Astals Cid <aacid@kde.org>
// Copyright (C) 2008 Kjartan Maraas <kmaraas@gnome.org>
// Copyright (C) 2008 Boris Toloknov <tlknv@yandex.ru>
// Copyright (C) 2008 Haruyuki Kawabe <Haruyuki.Kawabe@unisys.co.jp>
// Copyright (C) 2008 Tomas Are Haavet <tomasare@gmail.com>
// Copyright (C) 2009 Warren Toomey <wkt@tuhs.org>
// Copyright (C) 2009, 2011 Carlos Garcia Campos <carlosgc@gnome.org>
// Copyright (C) 2009 Reece Dunn <msclrhd@gmail.com>
// Copyright (C) 2010, 2012, 2013 Adrian Johnson <ajohnson@redneon.com>
// Copyright (C) 2010 Hib Eris <hib@hiberis.nl>
// Copyright (C) 2010 OSSD CDAC Mumbai by Leena Chourey (leenac@cdacmumbai.in) and Onkar Potdar (onkar@cdacmumbai.in)
// Copyright (C) 2011 Joshua Richardson <jric@chegg.com>
// Copyright (C) 2011 Stephen Reichling <sreichling@chegg.com>
// Copyright (C) 2011, 2012 Igor Slepchin <igor.slepchin@gmail.com>
// Copyright (C) 2012 Ihar Filipau <thephilips@gmail.com>
// Copyright (C) 2012 Gerald Schmidt <solahcin@gmail.com>
// Copyright (C) 2012 Pino Toscano <pino@kde.org>
// Copyright (C) 2013 Thomas Freitag <Thomas.Freitag@alfa.de>
// Copyright (C) 2013 Julien Nabet <serval2412@yahoo.fr>
// Copyright (C) 2013 Johannes Brandstätter <jbrandstaetter@gmail.com>
// Copyright (C) 2014 Fabio D'Urso <fabiodurso@hotmail.it>
//
// To see a description of the changes please see the Changelog file that
// came with your tarball or type make ChangeLog if you are building from git
//
//========================================================================

#ifdef __GNUC__
#pragma implementation
#endif

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <ctype.h>
#include <math.h>
#include <iostream>
#include <vector>
#include <regex>
#include "goo/GooString.h"
#include "goo/GooList.h"
#include "UnicodeMap.h"
#include "goo/gmem.h"
#include "Error.h"
#include "GfxState.h"
#include "Page.h"
#include "Annot.h"
#include "GlobalParams.h"
#include "XmlOutputDev.h"
#include "XmlFonts.h"
#include "HtmlUtils.h"
#include "Outline.h"
#include "PDFDoc.h"
#include "PreprocessDev.h"

#define DEBUG __FILE__ << ": " << __LINE__ << ": DEBUG: "
#define EPS 1e-13

bool dEqual(double a, double b) {
    return fabs(a - b) <= EPS;
}

std::regex _canBeChar("^[^a-z]+$");

bool canBeCharacter(char *s) {
    return std::regex_match(s, _canBeChar);
}

class HtmlImage {
public:
    HtmlImage(GooString *_fName, GfxState *state)
            : fName(_fName) {
        state->transform(0, 0, &xMin, &yMax);
        state->transform(1, 1, &xMax, &yMin);
    }

    ~HtmlImage() { delete fName; }

    double xMin, xMax;        // image x coordinates
    double yMin, yMax;        // image y coordinates
    GooString *fName;        // image file name
};

// returns true if x is closer to y than x is to z
static inline bool IS_CLOSER(float x, float y, float z) { return fabs((x) - (y)) < fabs((x) - (z)); }

extern GBool ignore;
extern GBool printCommands;
extern GBool printHtml;
extern GBool stout;
extern GBool showHidden;

extern double wordBreakThreshold;

static GBool debug = gFalse;
static GooString *gstr_buff0 = NULL; // a workspace in which I format strings

static GooString *basename(GooString *str) {

    char *p = str->getCString();
    int len = str->getLength();
    for (int i = len - 1; i >= 0; i--)
        if (*(p + i) == SLASH)
            return new GooString((p + i + 1), len - i - 1);
    return new GooString(str);
}

#if 0
static GooString* Dirname(GooString* str){

  char *p=str->getCString();
  int len=str->getLength();
  for (int i=len-1;i>=0;i--)
    if (*(p+i)==SLASH)
      return new GooString(p,i+1);
  return new GooString();
}
#endif

static const char *print_matrix(const double *mat) {
    delete gstr_buff0;

    gstr_buff0 = GooString::format("[{0:g} {1:g} {2:g} {3:g} {4:g} {5:g}]",
                                   *mat, mat[1], mat[2], mat[3], mat[4], mat[5]);
    return gstr_buff0->getCString();
}

static const char *print_uni_str(const Unicode *u, const unsigned uLen) {
    GooString *gstr_buff1 = NULL;

    delete gstr_buff0;

    if (!uLen) return "";
    gstr_buff0 = GooString::format("{0:c}", (*u < 0x7F ? *u & 0xFF : '?'));
    for (unsigned i = 1; i < uLen; i++) {
        if (u[i] < 0x7F) {
            gstr_buff1 = gstr_buff0->append(u[i] < 0x7F ? static_cast<char>(u[i]) & 0xFF : '?');
            delete gstr_buff0;
            gstr_buff0 = gstr_buff1;
        }
    }

    return gstr_buff0->getCString();
}

//------------------------------------------------------------------------
// HtmlString
//------------------------------------------------------------------------

HtmlString::HtmlString(GfxState *state, double fontSize, HtmlFontAccu *_fonts) : fonts(_fonts) {
    GfxFont *font;
    double x, y;

    state->transform(state->getCurX(), state->getCurY(), &x, &y);
    if ((font = state->getFont())) {
        double ascent = font->getAscent();
        double descent = font->getDescent();
        if (ascent > 1.05) {
            //printf( "ascent=%.15g is too high, descent=%.15g\n", ascent, descent );
            ascent = 1.05;
        }
        if (descent < -0.4) {
            //printf( "descent %.15g is too low, ascent=%.15g\n", descent, ascent );
            descent = -0.4;
        }
        yMin = y - ascent * fontSize;
        yMax = y - descent * fontSize;
        GfxRGB rgb;
        state->getFillRGB(&rgb);
        HtmlFont hfont = HtmlFont(font, static_cast<int>(fontSize - 1), rgb);
        if (isMatRotOrSkew(state->getTextMat())) {
            double normalizedMatrix[4];
            memcpy(normalizedMatrix, state->getTextMat(), sizeof(normalizedMatrix));
            // browser rotates the opposite way
            // so flip the sign of the angle -> sin() components change sign
            if (debug)
                std::cerr << DEBUG << "before transform: " << print_matrix(normalizedMatrix) << std::endl;
            normalizedMatrix[1] *= -1;
            normalizedMatrix[2] *= -1;
            if (debug)
                std::cerr << DEBUG << "after reflecting angle: " << print_matrix(normalizedMatrix) << std::endl;
            normalizeRotMat(normalizedMatrix);
            if (debug)
                std::cerr << DEBUG << "after norm: " << print_matrix(normalizedMatrix) << std::endl;
            hfont.setRotMat(normalizedMatrix);
        }
        fontpos = fonts->AddFont(hfont);
    } else {
        // this means that the PDF file draws text without a current font,
        // which should never happen
        yMin = y - 0.95 * fontSize;
        yMax = y + 0.35 * fontSize;
        fontpos = 0;
    }
    if (yMin == yMax) {
        // this is a sanity check for a case that shouldn't happen -- but
        // if it does happen, we want to avoid dividing by zero later
        yMin = y;
        yMax = y + 1;
    }
    col = 0;
    text = NULL;
    xRight = NULL;
    link = NULL;
    isUnderlined = gFalse;
    len = size = 0;
    yxNext = NULL;
    xyNext = NULL;
    htext = new GooString();
    dir = textDirUnknown;
}


HtmlString::~HtmlString() {
    gfree(text);
    delete htext;
    gfree(xRight);
}

void HtmlString::addChar(GfxState *state, double x, double y,
                         double dx, double dy, Unicode u) {
    if (dir == textDirUnknown) {
        //dir = UnicodeMap::getDirection(u);
        dir = textDirLeftRight;
    }

    if (len == size) {
        size += 16;
        text = (Unicode *) grealloc(text, size * sizeof(Unicode));
        xRight = (double *) grealloc(xRight, size * sizeof(double));
    }
    text[len] = u;
    if (len == 0) {
        xMin = x;
    }
    xMax = xRight[len] = x + dx;
//printf("added char: %f %f xright = %f\n", x, dx, x+dx);
    ++len;
}

void HtmlString::endString() {
    if (dir == textDirRightLeft && len > 1) {
        //printf("will reverse!\n");
        for (int i = 0; i < len / 2; i++) {
            Unicode ch = text[i];
            text[i] = text[len - i - 1];
            text[len - i - 1] = ch;
        }
    }
}

//------------------------------------------------------------------------
// HtmlPage
//------------------------------------------------------------------------

HtmlPage::HtmlPage(GBool rawOrder, char *imgExtVal) {
    this->rawOrder = rawOrder;
    curStr = NULL;
    yxStrings = NULL;
    xyStrings = NULL;
    yxCur1 = yxCur2 = NULL;
    fonts = new HtmlFontAccu();
    links = new HtmlLinks();
    underlineds = new XmlUnderlineds();
    imgList = new GooList();
    pageWidth = 0;
    pageHeight = 0;
    fontsPageMarker = 0;
    DocName = NULL;
    firstPage = -1;
    imgExt = new GooString(imgExtVal);
}

HtmlPage::~HtmlPage() {
    clear();
    delete DocName;
    delete fonts;
    delete links;
    delete underlineds;
    delete imgExt;
    deleteGooList(imgList, HtmlImage);
}

void HtmlPage::updateFont(GfxState *state) {
    GfxFont *font;
    double *fm;
    char *name;
    int code;
    double w;

    // adjust the font size
    fontSize = state->getTransformedFontSize();
    if ((font = state->getFont()) && font->getType() == fontType3) {
        // This is a hack which makes it possible to deal with some Type 3
        // fonts.  The problem is that it's impossible to know what the
        // base coordinate system used in the font is without actually
        // rendering the font.  This code tries to guess by looking at the
        // width of the character 'm' (which breaks if the font is a
        // subset that doesn't contain 'm').
        for (code = 0; code < 256; ++code) {
            if ((name = ((Gfx8BitFont *) font)->getCharName(code)) &&
                name[0] == 'm' && name[1] == '\0') {
                break;
            }
        }
        if (code < 256) {
            w = ((Gfx8BitFont *) font)->getWidth(code);
            if (w != 0) {
                // 600 is a generic average 'm' width -- yes, this is a hack
                fontSize *= w / 0.6;
            }
        }
        fm = font->getFontMatrix();
        if (fm[0] != 0) {
            fontSize *= fabs(fm[3] / fm[0]);
        }
    }
}

void HtmlPage::beginString(GfxState *state, GooString *s) {
    curStr = new HtmlString(state, fontSize, fonts);
}


void HtmlPage::conv() {
    HtmlString *tmp;

    int linkIndex = 0;
    HtmlFont *h;
    for (tmp = yxStrings; tmp; tmp = tmp->yxNext) {
        int pos = tmp->fontpos;
        //  printf("%d\n",pos);
        h = fonts->Get(pos);

        if (tmp->htext) delete tmp->htext;
        tmp->htext = HtmlFont::simple(h, tmp->text, tmp->len);

        if (underlineds->inUnderlined(tmp->xMin, tmp->yMin, tmp->xMax, tmp->yMax)) {
            tmp->isUnderlined = true;
        }

        if (links->inLink(tmp->xMin, tmp->yMin, tmp->xMax, tmp->yMax, linkIndex)) {
            tmp->link = links->getLink(linkIndex);
            /*GooString *t=tmp->htext;
            tmp->htext=links->getLink(k)->Link(tmp->htext);
            delete t;*/
        }
    }

}


void HtmlPage::addChar(GfxState *state, double x, double y,
                       double dx, double dy,
                       double ox, double oy, Unicode *u, int uLen) {
    double x1, y1, w1, h1, dx2, dy2;
    int n, i;
    state->transform(x, y, &x1, &y1);
    n = curStr->len;

    // check that new character is in the same direction as current string
    // and is not too far away from it before adding
    //if ((UnicodeMap::getDirection(u[0]) != curStr->dir) ||
    // XXX
    if (debug) {
        double *text_mat = state->getTextMat();
        // rotation is (cos q, sin q, -sin q, cos q, 0, 0)
        // sin q is zero iff there is no rotation, or 180 deg. rotation;
        // for 180 rotation, cos q will be negative
        if (text_mat[0] < 0 || !is_within(text_mat[1], .1, 0)) {
            std::cerr << DEBUG << "rotation matrix for \"" << print_uni_str(u, uLen) << '"' << std::endl;
            std::cerr << "text " << print_matrix(state->getTextMat());
        }
    }
    if (n > 0 && // don't start a new string, unless there is already a string
        // TODO: the following line assumes that text is flowing left to
        // right, which will not necessarily be the case, e.g. if rotated;
        // It assesses whether or not two characters are close enough to
        // be part of the same string
        fabs(x1 - curStr->xRight[n - 1]) > wordBreakThreshold * (curStr->yMax - curStr->yMin) &&
        // rotation is (cos q, sin q, -sin q, cos q, 0, 0)
        // sin q is zero iff there is no rotation, or 180 deg. rotation;
        // for 180 rotation, cos q will be negative
        !rot_matrices_equal(curStr->getFont().getRotMat(), state->getTextMat())) {
        endString();
        beginString(state, NULL);
    }
    state->textTransformDelta(state->getCharSpace() * state->getHorizScaling(),
                              0, &dx2, &dy2);
    dx -= dx2;
    dy -= dy2;
    state->transformDelta(dx, dy, &w1, &h1);
    if (uLen != 0) {
        w1 /= uLen;
        h1 /= uLen;
    }
    for (i = 0; i < uLen; ++i) {
        curStr->addChar(state, x1 + i * w1, y1 + i * h1, w1, h1, u[i]);
    }
}

void HtmlPage::endString() {
    HtmlString *p1, *p2;
    double h, y1, y2;

    // throw away zero-length strings -- they don't have valid xMin/xMax
    // values, and they're useless anyway
    if (curStr->len == 0) {
        delete curStr;
        curStr = NULL;
        return;
    }

    curStr->endString();

#if 0 //~tmp
    if (curStr->yMax - curStr->yMin > 20) {
      delete curStr;
      curStr = NULL;
      return;
    }
#endif

    // insert string in y-major list
    h = curStr->yMax - curStr->yMin;
    y1 = curStr->yMin + 0.5 * h;
    y2 = curStr->yMin + 0.8 * h;
    if (rawOrder) {
        p1 = yxCur1;
        p2 = NULL;
    } else if ((!yxCur1 ||
                (y1 >= yxCur1->yMin &&
                 (y2 >= yxCur1->yMax || curStr->xMax >= yxCur1->xMin))) &&
               (!yxCur2 ||
                (y1 < yxCur2->yMin ||
                 (y2 < yxCur2->yMax && curStr->xMax < yxCur2->xMin)))) {
        p1 = yxCur1;
        p2 = yxCur2;
    } else {
        for (p1 = NULL, p2 = yxStrings; p2; p1 = p2, p2 = p2->yxNext) {
            if (y1 < p2->yMin || (y2 < p2->yMax && curStr->xMax < p2->xMin))
                break;
        }
        yxCur2 = p2;
    }
    yxCur1 = curStr;
    if (p1)
        p1->yxNext = curStr;
    else
        yxStrings = curStr;
    curStr->yxNext = p2;
    curStr = NULL;
}

static const char *strrstr(const char *s, const char *ss) {
    const char *p = strstr(s, ss);
    for (const char *pp = p; pp != NULL; pp = strstr(p + 1, ss)) {
        p = pp;
    }
    return p;
}

static void CloseTags(GooString *htext, GBool &finish_a,
                      GBool &finish_underlined, GBool &finish_italic, GBool &finish_bold) {
    const char *last_underlined =
            finish_underlined && (finish_bold || finish_italic || finish_a) ? strrstr(htext->getCString(), "<u>")
                                                                            : NULL;
    const char *last_italic =
            finish_italic && (finish_bold || last_underlined || finish_a) ? strrstr(htext->getCString(), "<i>") : NULL;
    const char *last_bold =
            finish_bold && (finish_italic || last_underlined || finish_a) ? strrstr(htext->getCString(), "<b>") : NULL;
    const char *last_a =
            finish_a && (last_underlined || finish_italic || finish_bold) ? strrstr(htext->getCString(), "<a ") : NULL;

    if (finish_a && (finish_italic || finish_bold) && last_a > (last_italic > last_bold ? (last_italic > last_underlined
                                                                                           ? last_italic
                                                                                           : last_underlined)
                                                                                        : last_bold)) {
        htext->append("</a>", 4);
        finish_a = false;
    }
    if (finish_underlined && (finish_italic || finish_bold) &&
        last_underlined > (last_italic > last_bold ? last_italic : last_bold)) {
        htext->append("</u>", 4);
        finish_underlined = false;
    }
    if (finish_italic && finish_bold && last_italic > last_bold) {
        htext->append("</i>", 4);
        finish_italic = false;
    }
    if (finish_underlined && finish_bold && last_underlined > last_bold) {
        htext->append("</u>", 4);
        finish_underlined = false;
    }
    if (finish_bold)
        htext->append("</b>", 4);
    if (finish_italic)
        htext->append("</i>", 4);
    if (finish_underlined)
        htext->append("</u>", 4);
    if (finish_a)
        htext->append("</a>");
}

// Strings are lines of text;
// This function aims to combine strings into lines and paragraphs if gFalse
// It may also strip out duplicate strings (if they are on top of each other); sometimes they are to create a font effect
void HtmlPage::coalesce() {
    HtmlString *str1, *str2;
    HtmlFont *hfont1, *hfont2;
    double space, horSpace, vertSpace, vertOverlap;
    GBool addSpace, addLineBreak;
    int n, i;
    double curX, curY;

#if 0 //~ for debugging
    for (str1 = yxStrings; str1; str1 = str1->yxNext) {
      printf("x=%f..%f  y=%f..%f  size=%2d '",
         str1->xMin, str1->xMax, str1->yMin, str1->yMax,
         (int)(str1->yMax - str1->yMin));
      for (i = 0; i < str1->len; ++i) {
        fputc(str1->text[i] & 0xff, stdout);
      }
      printf("'\n");
    }
    printf("\n------------------------------------------------------------\n\n");
#endif
    str1 = yxStrings;

    if (!str1) return;

    str1 = yxStrings;

    hfont1 = getFont(str1);
    if (hfont1->isBold())
        str1->htext->insert(0, "<b>", 3);
    if (hfont1->isItalic())
        str1->htext->insert(0, "<i>", 3);
    if (str1->isUnderlined)
        str1->htext->insert(0, "<u>", 3);
    if (str1->getLink() != NULL) {
        GooString *ls = str1->getLink()->getLinkStart();
        str1->htext->insert(0, ls);
        delete ls;
    }
    curX = str1->xMin;
    curY = str1->yMin;

    while (str1 && (str2 = str1->yxNext)) {
        hfont2 = getFont(str2);
        space = str1->yMax - str1->yMin; // the height of the font's bounding box
        horSpace = str2->xMin - str1->xMax;
        // if strings line up on left-hand side AND they are on subsequent lines, we need a line break
        addLineBreak = gFalse && (fabs(str1->xMin - str2->xMin) < 0.4) &&
                       IS_CLOSER(str2->yMax, str1->yMax + space, str1->yMax);
        vertSpace = str2->yMin - str1->yMax;

//printf("coalesce %d %d %f? ", str1->dir, str2->dir, d);

        if (str2->yMin >= str1->yMin && str2->yMin <= str1->yMax) {
            vertOverlap = str1->yMax - str2->yMin;
        } else if (str2->yMax >= str1->yMin && str2->yMax <= str1->yMax) {
            vertOverlap = str2->yMax - str1->yMin;
        } else {
            vertOverlap = 0;
        }

        // Combine strings if:
        //  They appear to be the same font (complex mode only) && going in the same direction AND at least one of the following:
        //  1.  They appear to be part of the same line of text
        //  2.  They appear to be subsequent lines of a paragraph
        //  We assume (1) or (2) above, respectively, based on:
        //  (1)  strings overlap vertically AND
        //       horizontal space between end of str1 and start of str2 is consistent with a single space or less;
        //       when rawOrder, the strings have to overlap vertically by at least 50%
        //  (2)  Strings flow down the page, but the space between them is not too great, and they are lined up on the left
        if (
                (
                        (
                                (
                                        (rawOrder && vertOverlap > 0.5 * space)
                                        ||
                                        (!rawOrder && str2->yMin < str1->yMax)
                                ) &&
                                (horSpace > -0.5 * space && horSpace < 2 * space) // HACK: 2 spaced user mistake
                        ) ||
                        (vertSpace >= 0 && vertSpace < 0.5 * space && addLineBreak)
                ) &&
                (gFalse || (hfont1->isEqualIgnoreBoldColor(*hfont2))) &&
                // in complex mode fonts must be the same, in other modes fonts do not metter
                str1->dir == str2->dir // text direction the same
                ) {
//      printf("yes\n");
            n = str1->len + str2->len;
            if ((addSpace = horSpace > wordBreakThreshold * space)) {
                ++n;
            }
            if (addLineBreak) {
                ++n;
            }

            str1->size = (n + 15) & ~15;
            str1->text = (Unicode *) grealloc(str1->text,
                                              str1->size * sizeof(Unicode));
            str1->xRight = (double *) grealloc(str1->xRight,
                                               str1->size * sizeof(double));
            if (addSpace) {
                str1->text[str1->len] = 0x20;
                str1->htext->append(" ");
                str1->xRight[str1->len] = str2->xMin;
                ++str1->len;
            }
            if (addLineBreak) {
                str1->text[str1->len] = '\n';
                str1->htext->append("<br/>");
                str1->xRight[str1->len] = str2->xMin;
                ++str1->len;
                str1->yMin = str2->yMin;
                str1->yMax = str2->yMax;
                str1->xMax = str2->xMax;
                int fontLineSize = hfont1->getLineSize();
                int curLineSize = (int) (vertSpace + space);
                if (curLineSize != fontLineSize) {
                    HtmlFont *newfnt = new HtmlFont(*hfont1);
                    newfnt->setLineSize(curLineSize);
                    str1->fontpos = fonts->AddFont(*newfnt);
                    delete newfnt;
                    hfont1 = getFont(str1);
                    // we have to reget hfont2 because it's location could have
                    // changed on resize
                    hfont2 = getFont(str2);
                }
            }
            for (i = 0; i < str2->len; ++i) {
                str1->text[str1->len] = str2->text[i];
                str1->xRight[str1->len] = str2->xRight[i];
                ++str1->len;
            }

            /* fix <i>, <b> if str1 and str2 differ and handle switch of links */
            HtmlLink *hlink1 = str1->getLink();
            HtmlLink *hlink2 = str2->getLink();
            bool switch_links = !hlink1 || !hlink2 || !hlink1->isEqualDest(*hlink2);
            GBool finish_a = switch_links && hlink1 != NULL;
            GBool finish_underlined = str1->isUnderlined && (!str2->isUnderlined || finish_a);
            GBool finish_italic = hfont1->isItalic() && (!hfont2->isItalic() || finish_a || finish_underlined);
            GBool finish_bold =
                    hfont1->isBold() && (!hfont2->isBold() || finish_a || finish_underlined || finish_italic);
            CloseTags(str1->htext, finish_a, finish_underlined, finish_italic, finish_bold);
            if (switch_links && hlink2 != NULL) {
                GooString *ls = hlink2->getLinkStart();
                str1->htext->append(ls);
                delete ls;
            }
            if ((!str1->isUnderlined || finish_underlined) && str2->isUnderlined)
                str1->htext->append("<u>", 3);
            if ((!hfont1->isItalic() || finish_italic) && hfont2->isItalic())
                str1->htext->append("<i>", 3);
            if ((!hfont1->isBold() || finish_bold) && hfont2->isBold())
                str1->htext->append("<b>", 3);


            str1->htext->append(str2->htext);
            // str1 now contains href for link of str2 (if it is defined)
            str1->link = str2->link;
            str1->isUnderlined = str2->isUnderlined;
            hfont1 = hfont2;
            if (str2->xMax > str1->xMax) {
                str1->xMax = str2->xMax;
            }
            if (str2->yMax > str1->yMax) {
                str1->yMax = str2->yMax;
            }
            str1->yxNext = str2->yxNext;
            delete str2;
        } else if (horSpace > -0.8 * space && str2->len == 1 && str2->htext->getChar(0) == '.') {
            // TODO: hacked
            str1->htext->append("\u0323");

            str1->yxNext = str2->yxNext;
            delete str2;
        } else { // keep strings separate
//      printf("no\n");
            GBool finish_a = str1->getLink() != NULL;
            GBool finish_bold = hfont1->isBold();
            GBool finish_italic = hfont1->isItalic();
            GBool finish_underlined = str1->isUnderlined;
            CloseTags(str1->htext, finish_a, finish_underlined, finish_italic, finish_bold);

            str1->xMin = curX;
            str1->yMin = curY;
            str1 = str2;
            curX = str1->xMin;
            curY = str1->yMin;
            hfont1 = hfont2;
            if (hfont1->isBold())
                str1->htext->insert(0, "<b>", 3);
            if (hfont1->isItalic())
                str1->htext->insert(0, "<i>", 3);
            if (str1->isUnderlined)
                str1->htext->insert(0, "<u>", 3);
            if (str1->getLink() != NULL) {
                GooString *ls = str1->getLink()->getLinkStart();
                str1->htext->insert(0, ls);
                delete ls;
            }
        }
    }
    str1->xMin = curX;
    str1->yMin = curY;

    GBool finish_bold = hfont1->isBold();
    GBool finish_italic = hfont1->isItalic();
    GBool finish_underlined = str1->isUnderlined;
    GBool finish_a = str1->getLink() != NULL;
    CloseTags(str1->htext, finish_a, finish_underlined, finish_italic, finish_bold);

#if 0 //~ for debugging
    for (str1 = yxStrings; str1; str1 = str1->yxNext) {
      printf("x=%3d..%3d  y=%3d..%3d  size=%2d ",
         (int)str1->xMin, (int)str1->xMax, (int)str1->yMin, (int)str1->yMax,
         (int)(str1->yMax - str1->yMin));
      printf("'%s'\n", str1->htext->getCString());
    }
    printf("\n------------------------------------------------------------\n\n");
#endif

}

enum ParagraphFormat {
    UNKNOWN, LEFT, CENTER, RIGHT
};

static const char *ParagraphFormatStrings[] = {"unknown", "left", "center", "right"};

enum ParagraphType {
    GENERAL,
    TRANSITION,
    SCENE_HEADING,
    ACTION,
    CHARACTER,
    CHARACTER_LEFT,
    CHARACTER_RIGHT,
    PARENTHETICAL,
    PARENTHETICAL_LEFT,
    PARENTHETICAL_RIGHT,
    DIALOGUE,
    DIALOGUE_LEFT,
    DIALOGUE_RIGHT,
    PAGE_NUMBER,
    PAGE_HEADER
};

static const char *ParagraphTypeStrings[] = {
        "general", "transition", "scene_heading", "action",
        "character", "character_left", "character_right",
        "parenthetical", "parenthetical_left", "parenthetical_right",
        "dialogue", "dialogue_left", "dialogue_right",
        "page_number", "page_header"
};

struct Paragraph {
    ParagraphFormat format = UNKNOWN;
    ParagraphType type = GENERAL;
    int paddingLeft = 0, relativeTop = 0, top = 0;
    double paddingRight = 0;
    std::vector<HtmlString *> lines;

    bool canInsertBreakBefore(Paragraph last) {
        return last.type == CHARACTER_RIGHT ||
               last.type == PARENTHETICAL_RIGHT ||
               last.type == DIALOGUE_RIGHT ||
               (last.type == CHARACTER_LEFT && type != CHARACTER_RIGHT) ||
               (last.type == PARENTHETICAL_LEFT && type != PARENTHETICAL_RIGHT) ||
               (last.type == DIALOGUE_LEFT && type != DIALOGUE_RIGHT);
    }

    void reset() {
        format = UNKNOWN;
        type = GENERAL;

        paddingLeft = 0;
        paddingRight = 0;
        relativeTop = 0;

        top = 0;

        lines.clear();
    }
};

bool isPrefix(const char *prefix, char *string) {
    const char *c = prefix, *s = string;

    while (*c) {
        if (!*s || *s != *c) {
            return false;
        }

        c++;
        s++;
    }

    return true;
}

class LineProcess {
private:
    static bool LeftCompare(HtmlPage *page, HtmlString *line, HtmlString *nextLine, Paragraph &paragraph,
                            PreprocessDev *parameter) {
        if (!nextLine) {
            return false;
        }

        bool sameParagraph = IsNextLine(line, nextLine, parameter);

        if (sameParagraph && (int) nextLine->xMin == paragraph.paddingLeft) {
            GuessFormat(page, line, paragraph, parameter);

            paragraph.format = LEFT;

            if (!dEqual(page->pageWidth - line->xMax, paragraph.paddingRight)) {
                paragraph.paddingRight = -1;
            }

            return true;
        }

        return false;
    }

    static bool RightCompare(HtmlPage *page, HtmlString *line, HtmlString *nextLine, Paragraph &paragraph,
                             PreprocessDev *parameter) {
        if (!nextLine) {
            return false;
        }

        bool sameParagraph = IsNextLine(line, nextLine, parameter);

        if (sameParagraph && dEqual(page->pageWidth - nextLine->xMax, paragraph.paddingRight)) {
            paragraph.format = RIGHT;

            if (line->xMin != paragraph.paddingLeft) {
                paragraph.paddingLeft = -1;
            }

            return true;
        }

        return false;
    }

    static bool CenterCompare(HtmlPage *page, HtmlString *line, HtmlString *nextLine, Paragraph &paragraph,
                              PreprocessDev *parameter) {
        if (!nextLine) {
            return false;
        }

        bool sameParagraph = IsNextLine(line, nextLine, parameter),
                isCenter = IsCenter(page, nextLine);

        if (canBeCharacter(line->htext->getCString())) {
            return false;
        }

        if (paragraph.format == CENTER && sameParagraph && isCenter) {
            if (line->xMin != paragraph.paddingLeft) {
                paragraph.paddingLeft = -1;
            }
            if (!dEqual(page->pageWidth - line->xMax, paragraph.paddingRight)) {
                paragraph.paddingRight = -1;
            }

            return true;
        }

        return false;
    }

    static bool ParentheticalCompare(HtmlPage *page, HtmlString *line, HtmlString *nextLine, Paragraph &paragraph,
                                     PreprocessDev *parameter) {
        if (!nextLine) {
            return false;
        }

        bool sameParagraph = IsNextLine(line, nextLine, parameter),
                isCenter = IsCenter(page, nextLine);

        if (paragraph.type == PARENTHETICAL && sameParagraph) {
            if (line->xMin != paragraph.paddingLeft) {
                paragraph.paddingLeft = -1;
            }
            if (!dEqual(page->pageWidth - line->xMax, paragraph.paddingRight)) {
                paragraph.paddingRight = -1;
            }

            char *text = line->htext->getCString(),
                    c = text[line->htext->getLength() - 1];

            return c != ')';
        }

        return false;
    }

    static bool IsPageNumber(HtmlPage *page, HtmlString *line) {
        return IsPageNumber(line);
    }

    static bool IsParenthetical(HtmlPage *page, HtmlString *line) {
        char *text = line->htext->getCString();
        return *(text) == '(' && line->xMin > page->pageWidth / 4;
    }

    static bool IsCenter(HtmlPage *page, HtmlString *line) {
        return fabs(line->xMin + line->xMax - page->pageWidth) < 2;
    }

    static bool IsRight(HtmlPage *page, HtmlString *line) {
        return line->xMin > 2 * (page->pageWidth - line->xMax);
    }

    static bool IsNextLine(HtmlString *line, HtmlString *nextLine, PreprocessDev *parameter) {
        double lineSpacing = nextLine->yMin - line->yMin;

        return parameter->getSpacing() > lineSpacing && lineSpacing >= 0;
    }

    static bool GuessFormat(HtmlPage *page, HtmlString *line, Paragraph &paragraph, PreprocessDev *parameter) {
        if (paragraph.format == UNKNOWN) {
            paragraph.paddingLeft = (int) line->xMin;
            paragraph.paddingRight = page->pageWidth - line->xMax;

            paragraph.top = (int) line->yMin;

            if (line->yMin < parameter->getMarginTop()) {
                paragraph.type = PAGE_HEADER;
                if (IsCenter(page, line)) {
                    paragraph.format = LEFT;
                } else if (IsRight(page, line)) {
                    paragraph.format = RIGHT;
                } else {
                    paragraph.format = LEFT;
                }
            } else if (IsPageNumber(page, line)) {
                paragraph.type = PAGE_NUMBER;
            } else if (IsParenthetical(page, line)) {
                paragraph.format = LEFT;
                paragraph.type = PARENTHETICAL;
            } else if (paragraph.paddingLeft == parameter->getMarginLeft()) {
                paragraph.format = LEFT;
            } else if (IsCenter(page, line)) {
                paragraph.format = CENTER;
            } else if (IsRight(page, line)) {
                paragraph.format = RIGHT;
            } else {
                paragraph.format = LEFT;
            }

            return true;
        }

        return false;
    }

public:
    static bool IsPageNumber(HtmlString *line) {
        std::string s = line->htext->getCString();
        std::regex p("\\s*([0-9]+|[ivx]+|[IVX]+)\\.");

        return std::regex_match(s, p);
    }

    static bool IsSameParagraph(HtmlPage *page, HtmlString *line, HtmlString *nextLine, Paragraph &paragraph,
                                PreprocessDev *parameter) {
        bool same = false;
        bool (*comparators[])(HtmlPage *, HtmlString *, HtmlString *, Paragraph &, PreprocessDev *) = {
                ParentheticalCompare,
                CenterCompare,
                LeftCompare,
                RightCompare
        };

        GuessFormat(page, line, paragraph, parameter);

        for (auto cmpFunc : comparators) {
            Paragraph _paragraph = paragraph;
            bool result = cmpFunc(page, line, nextLine, _paragraph, parameter);

            if (result) {
                same = true;
                paragraph = _paragraph;
                break;
            }
        }

        return same;
    }

    static ParagraphType GuessParagraphType(HtmlPage *page, Paragraph *lastParagraph, Paragraph *paragraph) {
        if (paragraph->type == PAGE_HEADER) {
            return PAGE_HEADER;
        }

        if (paragraph->type == PAGE_NUMBER) {
            return PAGE_NUMBER;
        }

        if (paragraph->type == PARENTHETICAL) {
            return PARENTHETICAL;
        }

        if (lastParagraph && (
                lastParagraph->type == PARENTHETICAL ||
                lastParagraph->type == CHARACTER)) {
            return DIALOGUE;
        }

        if (lastParagraph &&
            lastParagraph->type != CHARACTER &&
            paragraph->lines.size() == 1 &&
            canBeCharacter(paragraph->lines[0]->htext->getCString()) &&
            paragraph->format != RIGHT &&
            paragraph->paddingLeft > page->pageWidth / 4) {
            return CHARACTER;
        }

        if (paragraph->format == LEFT &&
            paragraph->paddingLeft < page->pageWidth / 4) {
            char *l = paragraph->lines[0]->htext->getCString();
            if (paragraph->lines.size() == 1 && (
                    isPrefix("INT.", l) ||
                    isPrefix("EXT.", l))) {
                return SCENE_HEADING;
            }

            return ACTION;
        }

        if (paragraph->format == RIGHT && paragraph->lines.size() == 1) {
            GooString *t = paragraph->lines[0]->htext;
            char *l = t->getCString();
            if (l[t->getLength() - 1] == ':') {
                return TRANSITION;
            }
        }

        if (lastParagraph == NULL &&
            paragraph->lines.size() == 1 &&
            paragraph->paddingLeft > page->pageWidth / 4 &&
            paragraph->paddingLeft < page->pageWidth / 2) {
            return CHARACTER;
        }

        return GENERAL;
    }
};

bool isAsteriskString(GooString *s) {
    int l = s->getLength();
    char *c = s->getCString();

    for (int i = 0; i < l; i++) {
        if (c[i] != '*') {
            return false;
        }
    }

    return true;
}

void HtmlPage::dumpAsBradsXML(FILE *f, int page, GBool &isCover, PreprocessDev *parameter) {
    std::vector<Paragraph> pars;
    Paragraph paragraph;

    int lineCount = 0;
    HtmlString *line = yxStrings;

    int lastY = 0;
    while (line) {
        if (line->htext) {
            if (lastY != (int) line->yMax) {
                lastY = (int) line->yMax;
                lineCount++;
            }
        }

        line = line->yxNext;
    }

    isCover = isCover && lineCount < 12;

    line = yxStrings;

    while (line &&
           (!line->htext ||
            isAsteriskString(line->htext) ||
            (!isCover && parameter && xoutRound(line->xMin) < parameter->getMarginLeft()))) {
        line = line->yxNext;
    }

    paragraph.relativeTop = line ? (int) line->yMin : 0;

    while (line) {
        HtmlString *nextLine = line->yxNext;

        while (nextLine &&
               (!nextLine->htext ||
                isAsteriskString(nextLine->htext) ||
                (!isCover && parameter && xoutRound(nextLine->xMin) < parameter->getMarginLeft()))) {
            nextLine = nextLine->yxNext;
        }

        bool sameParagraph = LineProcess::IsSameParagraph(this, line, nextLine, paragraph, parameter);

        if (paragraph.type == PAGE_HEADER && LineProcess::IsPageNumber(line)) {

        } else {
            paragraph.lines.push_back(line);
        }

        if (!sameParagraph) {
            if (paragraph.lines.size() > 0) pars.push_back(paragraph);
            paragraph.reset();

            if (nextLine) {
                paragraph.relativeTop = (int) (nextLine->yMin - line->yMax);
            }
        }

        line = nextLine;
    }

    Paragraph *lastPar = NULL;
    for (int i = 0; i < pars.size(); ++i) {
        Paragraph *p = &pars[i];

        p->type = LineProcess::GuessParagraphType(this, lastPar, p);

        lastPar = p;
    }

    /**
     * Check for dual dialogue
     */
    // TODO: fix hardcoded
    int pars_n = (int) pars.size() - 3;
    for (int i = 0; i < pars_n; ++i) {
        int _i = i;
        Paragraph *char1 = &pars[i],
                *dialog1 = &pars[i + 1],
                *char2 = &pars[i + 2],
                *dialog2 = &pars[i + 3],
                *parent1 = NULL,
                *parent2 = NULL;

        if (char1->type != CHARACTER) {
            continue;
        }

        if (dialog1->type == PARENTHETICAL) {
            if (++_i >= pars_n) {
                break;
            }

            parent1 = dialog1;
            dialog1++;
            char2++;
            dialog2++;
        }

        if (dialog2 != NULL && dialog2->type == PARENTHETICAL) {
            if (++_i >= pars_n) {
                break;
            }

            parent2 = dialog2;
            dialog2++;
        }

        if (dialog1->format == RIGHT && dialog2->format == RIGHT &&
            dialog1->lines.size() == 1 &&
            canBeCharacter(dialog1->lines.front()->htext->getCString())) {

            Paragraph *tmp = char2;
            char2 = dialog1;
            dialog1 = tmp;

            dialog1->type = DIALOGUE;
            char2->type = GENERAL;
            dialog2->type = GENERAL;
        }

        if (char1->type == CHARACTER &&
            dialog1->type == DIALOGUE &&
            char2->type == GENERAL &&
            dialog2->type == GENERAL &&
            char2->paddingLeft > char1->paddingLeft &&
            char2->paddingLeft > dialog2->paddingLeft) {
            char1->type = CHARACTER_LEFT;
            dialog1->type = DIALOGUE_LEFT;
            char2->type = CHARACTER_RIGHT;
            dialog2->type = DIALOGUE_RIGHT;

            if (parent1) parent1->type = PARENTHETICAL_LEFT;
            if (parent2) parent2->type = PARENTHETICAL_RIGHT;
        }
    }

#if 0
    fprintf(f, "<!-- number=\"%d\" height=\"%d\" width=\"%d\" -->\n",
            page, pageHeight, pageWidth);
#endif

    fprintf(f, "<Page No=\"%d\" height=\"%dpt\" width=\"%dpt\"",
            page, pageHeight, pageWidth);
    if (isCover) fputs(" class=\"cover\"", f);
    fputs(">\n", f);

    for (int i = fontsPageMarker; i < fonts->size(); i++) {
        GooString *fontCSStyle = fonts->CSStyle(i);
        fprintf(f, "\t<!-- %s -->\n", fontCSStyle->getCString());
        delete fontCSStyle;
    }

    bool firstPar = true;
    Paragraph lastP;

    for (auto p: pars) {
        // DUMP LINE

#if 0
        for (auto l: p.lines) {
            fprintf(f, "\t<!-- <Text top=\"%d\" left=\"%d\" ",
                    xoutRound(l->yMin),
                    xoutRound(l->xMin));
            fprintf(f, "bottom=\"%d\" right=\"%d\" ",
                    xoutRound(l->yMax),
                    xoutRound(l->xMax));
            fprintf(f, "width=\"%d\" height=\"%d\" ",
                    xoutRound(l->xMax - l->xMin),
                    xoutRound(l->yMax - l->yMin));
            fprintf(f, "font=\"%d\">", l->fontpos);
            fputs(l->htext->getCString(), f);
            fputs("</Text> -->\n", f);
        }
#endif

        bool samePar = lastP.top == p.top &&
                       lastP.type != CHARACTER_LEFT &&
                       lastP.type != DIALOGUE_LEFT;

        if (samePar) {
            fputs("\t\t<span", f);

            if (p.format == RIGHT) {
                fputs(" style=\"float: right;\"", f);
            }

            fputs(">", f);
        } else {
            if (firstPar) {
                firstPar = false;
            } else {
                fputs("\t</Paragraph>\n", f);
            }

            if (!isCover && p.canInsertBreakBefore(lastP)) {
                fputs("\t<Paragraph class=\"dual_break\"/>\n", f);
            }

            fputs("\t<Paragraph", f);

            if ((p.type == GENERAL || isCover) && p.type != PAGE_NUMBER) {
                fprintf(f, " text-align=\"%s\"", ParagraphFormatStrings[p.format]);
                if (p.format == LEFT) {
                    fprintf(f, " padding-left=\"%dpt\"", p.paddingLeft);
                } else if (p.format == RIGHT) {
                    fprintf(f, " padding-right=\"%dpt\"", (int) p.paddingRight);
                }
                if (isCover) {
                    fprintf(f, " padding-top=\"%dpt\"", p.relativeTop);
                }
            } else {
                fprintf(f, " class=\"%s\"", ParagraphTypeStrings[p.type]);
            }

            fputs(">\n\t\t<span>", f);
        }

        bool isFirstLine = true,
                lastLineSpace = true;

        std::string pendingTag = "";
        for (auto l: p.lines) {
            if (p.format == CENTER && !isFirstLine) {
                fputs("<br />", f);
            }

            std::string s = l->htext->getCString();
            if (pendingTag.length() > 0) {
                std::regex startTag("^<" + pendingTag + ">.+");
                if (std::regex_match(s, startTag)) {
                    l->htext->del(0, (int) pendingTag.length() + 2);
                } else {
                    auto tag = "</" + pendingTag + ">";
                    fputs(tag.c_str(), f);
                }
            }

            char firstChar = l->htext->getChar(0);
            bool curLineSpace = (firstChar == ' ' || firstChar == '-');
            if (!lastLineSpace && !curLineSpace) {
                fputs(" ", f);
            }

            std::regex endTag("</([^>]+)>$");
            std::smatch m;
            if (std::regex_search(s, m, endTag)) {
                pendingTag = m[1];
                auto n = (int) pendingTag.length() + 3;
                auto i = l->htext->getLength() - n;
                l->htext->del(i, n);
            } else {
                pendingTag = "";
            }

            fputs(l->htext->getCString(), f);

            isFirstLine = false;

            char lastChar = l->htext->getChar(l->htext->getLength() - 1);
            lastLineSpace = (lastChar == ' ' || lastChar == '-');
        }

        if (pendingTag.length() > 0) {
            auto tag = "</" + pendingTag + ">";
            fputs(tag.c_str(), f);
        }

        fputs("</span>\n", f);

        lastP = p;
    }

    if (!firstPar) fputs("\t</Paragraph>\n", f);
    fputs("</Page>\n", f);
}

void HtmlPage::clear() {
    HtmlString *p1, *p2;

    if (curStr) {
        delete curStr;
        curStr = NULL;
    }
    for (p1 = yxStrings; p1; p1 = p2) {
        p2 = p1->yxNext;
        delete p1;
    }
    yxStrings = NULL;
    xyStrings = NULL;
    yxCur1 = yxCur2 = NULL;

    fontsPageMarker = fonts->size();

    delete links;
    links = new HtmlLinks();

    delete underlineds;
    underlineds = new XmlUnderlineds();
}

void HtmlPage::setDocName(char *fname) {
    DocName = new GooString(fname);
}

void HtmlPage::addImage(GooString *fname, GfxState *state) {
    HtmlImage *img = new HtmlImage(fname, state);
    imgList->append(img);
}

//------------------------------------------------------------------------
// XmlOutputDev
//------------------------------------------------------------------------

static const char *HtmlEncodings[][2] = {
        {"Latin1", "ISO-8859-1"},
        {NULL, NULL}
};

GooString *XmlOutputDev::mapEncodingToHtml(GooString *encoding) {
    GooString *enc = encoding;
    for (int i = 0; HtmlEncodings[i][0] != NULL; i++) {
        if (enc->cmp(HtmlEncodings[i][0]) == 0) {
            delete enc;
            return new GooString(HtmlEncodings[i][1]);
        }
    }
    return enc;
}

XmlOutputDev::XmlOutputDev(Catalog *catalogA, char *fileName, char *title,
                           char *author, char *keywords, char *subject, char *date,
                           char *extension,
                           GBool rawOrder, int firstPage,
                           PreprocessDev *preprocessor) {
    catalog = catalogA;
    docTitle = new GooString(title);
    pages = NULL;
    dumpJPEG = gTrue;
    pageCover = gTrue;
    //write = gTrue;
    this->rawOrder = rawOrder;
    ok = gFalse;
    //this->firstPage = firstPage;
    //pageNum=firstPage;
    // open file
    needClose = gFalse;
    pages = new HtmlPage(rawOrder, extension);

    parameter = preprocessor;

    maxPageWidth = 0;
    maxPageHeight = 0;

    pages->setDocName(fileName);
    Docname = new GooString(fileName);

    if (stout) page = stdout;
    else {
        GooString *right = new GooString(fileName);
        right->append(".xml");
        if (!(page = fopen(right->getCString(), "w"))) {
            error(errIO, -1, "Couldn't open xml file '{0:t}'", right);
            delete right;
            return;
        }
        delete right;
    }

    GooString *htmlEncoding = mapEncodingToHtml(globalParams->getTextEncodingName());
    fprintf(page, "<?xml version=\"1.0\" encoding=\"%s\" standalone=\"no\"?>\n", htmlEncoding->getCString());
    fputs("<Brads DocumentType=\"Script\">\n", page);
    fputs("<Content>\n", page);

    delete htmlEncoding;

    ok = gTrue;
}

XmlOutputDev::~XmlOutputDev() {
    HtmlFont::clear();

    delete Docname;
    delete docTitle;

//    deleteGooList(glMetaVars, HtmlMetaVar);

    if (page != NULL) {
        fputs("</Content>\n", page);
        fputs("</Brads>\n", page);
        fclose(page);
    }
    if (pages)
        delete pages;
}

void XmlOutputDev::startPage(int pageNum, GfxState *state, XRef *xref) {
#if 0
    if (mode&&!xml){
      if (write){
        write=gFalse;
        GooString* fname=Dirname(Docname);
        fname->append("image.log");
        if((tin=fopen(getFileNameFromPath(fname->getCString(),fname->getLength()),"w"))==NULL){
      printf("Error : can not open %s",fname);
      exit(1);
        }
        delete fname;
      // if(state->getRotation()!=0)
      //  fprintf(tin,"ROTATE=%d rotate %d neg %d neg translate\n",state->getRotation(),state->getX1(),-state->getY1());
      // else
        fprintf(tin,"ROTATE=%d neg %d neg translate\n",state->getX1(),state->getY1());
      }
    }
#endif

    this->pageNum = pageNum;
    GooString *str = basename(Docname);
    pages->clear();

    pages->pageWidth = static_cast<int>(state->getPageWidth());
    pages->pageHeight = static_cast<int>(state->getPageHeight());

    delete str;
}


void XmlOutputDev::endPage() {
    Links *linksList = docPage->getLinks();
    for (int i = 0; i < linksList->getNumLinks(); ++i) {
        doProcessLink(linksList->getLink(i));
    }
    delete linksList;

    pages->conv();
    pages->coalesce();
    pages->dumpAsBradsXML(page, pageNum, pageCover, parameter);

    // I don't yet know what to do in the case when there are pages of different
    // sizes and we want complex output: running ghostscript many times
    // seems very inefficient. So for now I'll just use last page's size
    maxPageWidth = pages->pageWidth;
    maxPageHeight = pages->pageHeight;

    //if(gFalse&&!xml) fputs("<br/>\n", fContentsFrame);
    if (!stout && !globalParams->getErrQuiet()) printf("Page-%d\n", (pageNum));
}

void XmlOutputDev::updateFont(GfxState *state) {
    pages->updateFont(state);
}

void XmlOutputDev::beginString(GfxState *state, GooString *s) {
    pages->beginString(state, s);
}

void XmlOutputDev::endString(GfxState *state) {
    pages->endString();
}

void XmlOutputDev::drawChar(GfxState *state, double x, double y,
                            double dx, double dy,
                            double originX, double originY,
                            CharCode code, int /*nBytes*/, Unicode *u, int uLen) {
    if (!showHidden && (state->getRender() & 3) == 3) {
        return;
    }
    pages->addChar(state, x, y, dx, dy, originX, originY, u, uLen);
}

void XmlOutputDev::stroke(GfxState *state) {
    GfxPath *path;
    GfxSubpath *subpath;
    double x[2], y[2];

    path = state->getPath();
    if (path->getNumSubpaths() != 1) {
        return;
    }
    subpath = path->getSubpath(0);
    if (subpath->getNumPoints() != 2) {
        return;
    }
    state->transform(subpath->getX(0), subpath->getY(0), &x[0], &y[0]);
    state->transform(subpath->getX(1), subpath->getY(1), &x[1], &y[1]);

    // look for a vertical or horizontal line
    if (x[0] == x[1] || y[0] == y[1]) {
        XmlUnderlined t(x[0], y[0], x[1], y[1]);
        pages->addUnderline(t);
    }
}

void XmlOutputDev::fill(GfxState *state) {
    GfxRGB rgb;
    state->getFillRGB(&rgb);

    if (rgb.r == 65536 && rgb.g == 65536 && rgb.b == 65536) {
        return;
    }

    GfxPath *path;
    GfxSubpath *subpath;
    double x[5], y[5];

    path = state->getPath();
    if (path->getNumSubpaths() != 1) {
        return;
    }
    subpath = path->getSubpath(0);
    if (subpath->getNumPoints() != 5) {
        return;
    }
    state->transform(subpath->getX(0), subpath->getY(0), &x[0], &y[0]);
    state->transform(subpath->getX(1), subpath->getY(1), &x[1], &y[1]);
    state->transform(subpath->getX(2), subpath->getY(2), &x[2], &y[2]);
    state->transform(subpath->getX(3), subpath->getY(3), &x[3], &y[3]);
    state->transform(subpath->getX(4), subpath->getY(4), &x[4], &y[4]);

    /**
     * horizontal line
     */
    double x1, x2, y1, y2, m, t;
    bool isRect = false;

    if (x[0] != x[4] && y[0] != y[4]) {
        return;
    }

    if (x[0] == x[1] && x[2] == x[3] && y[1] == y[2] && y[3] == y[4]) {
        isRect = true;
        x1 = x[0];
        x2 = x[2];
        y1 = y[1];
        y2 = y[3];
    } else if (x[1] == x[2] && x[3] == x[4] && y[0] == y[1] && y[2] == y[3]) {
        isRect = true;
        x1 = x[1];
        x2 = x[3];
        y1 = y[0];
        y2 = y[3];
    }

    /**
     * Normalize
     */
    if (x1 > x2) { t = x1; x1 = x2; x2 = t; }
    if (y1 > y2) { t = y1; y1 = y2; y2 = t; }

    if (isRect && y2 - y1 < 2) {
        m = (y1 + y2) / 2;
        XmlUnderlined t(x1, m, x2, m);
        pages->addUnderline(t);
    }

    /**
     * vertical line
     */
}

void XmlOutputDev::doProcessLink(AnnotLink *link) {
    double _x1, _y1, _x2, _y2;
    int x1, y1, x2, y2;

    link->getRect(&_x1, &_y1, &_x2, &_y2);
    cvtUserToDev(_x1, _y1, &x1, &y1);

    cvtUserToDev(_x2, _y2, &x2, &y2);


    GooString *_dest = getLinkDest(link);
    HtmlLink t((double) x1, (double) y2, (double) x2, (double) y1, _dest);
    pages->AddLink(t);
    delete _dest;
}

GooString *XmlOutputDev::getLinkDest(AnnotLink *link) {
    char *p;
    if (!link->getAction())
        return new GooString();
    switch (link->getAction()->getKind()) {
        case actionGoTo: {
            GooString *file = basename(Docname);
            int page = 1;
            LinkGoTo *ha = (LinkGoTo *) link->getAction();
            LinkDest *dest = NULL;
            if (ha->getDest() != NULL)
                dest = ha->getDest()->copy();
            else if (ha->getNamedDest() != NULL)
                dest = catalog->findDest(ha->getNamedDest());

            if (dest) {
                if (dest->isPageRef()) {
                    Ref pageref = dest->getPageRef();
                    page = catalog->findPage(pageref.num, pageref.gen);
                } else {
                    page = dest->getPageNum();
                }

                delete dest;

                GooString *str = GooString::fromInt(page);
                /* 		complex 	simple
                     frames		file-4.html	files.html#4
              gTrue	file.html#4	file.html#4
                 */

                file->append(".html#");
                file->append(str);

                if (printCommands) printf(" link to page %d ", page);
                delete str;
                return file;
            } else {
                return new GooString();
            }
        }
        case actionGoToR: {
            LinkGoToR *ha = (LinkGoToR *) link->getAction();
            LinkDest *dest = NULL;
            int page = 1;
            GooString *file = new GooString();
            if (ha->getFileName()) {
                delete file;
                file = new GooString(ha->getFileName()->getCString());
            }
            if (ha->getDest() != NULL) dest = ha->getDest()->copy();
            if (dest && file) {
                if (!(dest->isPageRef())) page = dest->getPageNum();
                delete dest;

                if (printCommands) printf(" link to page %d ", page);
                if (printHtml) {
                    p = file->getCString() + file->getLength() - 4;
                    if (!strcmp(p, ".pdf") || !strcmp(p, ".PDF")) {
                        file->del(file->getLength() - 4, 4);
                        file->append(".html");
                    }
                    file->append('#');
                    file->append(GooString::fromInt(page));
                }
            }
            if (printCommands && file) printf("filename %s\n", file->getCString());
            return file;
        }
        case actionURI: {
            LinkURI *ha = (LinkURI *) link->getAction();
            GooString *file = new GooString(ha->getURI()->getCString());
            // printf("uri : %s\n",file->getCString());
            return file;
        }
        case actionLaunch: {
            LinkLaunch *ha = (LinkLaunch *) link->getAction();
            GooString *file = new GooString(ha->getFileName()->getCString());
            if (printHtml) {
                p = file->getCString() + file->getLength() - 4;
                if (!strcmp(p, ".pdf") || !strcmp(p, ".PDF")) {
                    file->del(file->getLength() - 4, 4);
                    file->append(".html");
                }
                if (printCommands) printf("filename %s", file->getCString());

                return file;

            }
        }
        default:
            return new GooString();
    }
}
